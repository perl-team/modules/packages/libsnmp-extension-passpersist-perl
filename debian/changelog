libsnmp-extension-passpersist-perl (0.07-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove TANIGUCHI Takaki from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove constraints unnecessary since stretch:
    + Build-Depends: Drop versioned constraint on libmodule-build-perl.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 17 Jun 2022 10:28:52 +0100

libsnmp-extension-passpersist-perl (0.07-2.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Mon, 04 Jan 2021 16:07:54 +0100

libsnmp-extension-passpersist-perl (0.07-2) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Switch to "3.0 (quilt)" source format.
  * Add years of upstream copyright.
  * Unconditionally build depend on libmodule-build-perl.
  * Bump debhelper compatibility level to 9.
  * Drop unversioned perl from Depends.
  * Declare compliance with Debian Policy 3.9.6.
  * Mark package as autopkgtest-able.
  * Fix hashbangs in example scripts.
  * Add a patch to fix some spelling mistakes.

 -- gregor herrmann <gregoa@debian.org>  Fri, 29 May 2015 22:43:00 +0200

libsnmp-extension-passpersist-perl (0.07-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ TANIGUCHI Takaki ]
  * Imported Upstream version 0.07
  * debian/control: Bump Standards-Version to 3.9.4 (with no changes).

 -- TANIGUCHI Takaki <takaki@debian.org>  Thu, 06 Jun 2013 16:33:25 +0900

libsnmp-extension-passpersist-perl (0.06-1) unstable; urgency=low

  [ gregor herrmann ]
  * Correct versioned build dependency on libmodule-build-perl.

  [ TANIGUCHI Takaki ]
  * New upstream.
  * Bump Standards-Version to 3.9.2
  * add versioned debhelper to Build-Depends.

 -- TANIGUCHI Takaki <takaki@debian.org>  Fri, 19 Aug 2011 13:45:49 +0900

libsnmp-extension-passpersist-perl (0.05-2) unstable; urgency=low

  * debian/rules: make dh_auto_test dummy target due to fail test
    in chroot envirionment. (Closes: #617255)

 -- TANIGUCHI Takaki <takaki@debian.org>  Tue, 05 Apr 2011 22:34:33 +0900

libsnmp-extension-passpersist-perl (0.05-1) unstable; urgency=low

  * Initial Release. (Closes: #510071)

 -- TANIGUCHI Takaki <takaki@debian.org>  Fri, 04 Feb 2011 10:47:46 +0900
